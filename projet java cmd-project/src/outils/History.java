package outils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public  abstract class History {

	private static List<Cmd> hist = new ArrayList<>();
	private static List<String> date = new ArrayList<>();
	
	
	public void historiser() {
		
		if(hist.size() < 10) {
		
		hist.add(0, (Cmd) this);
		Date today = new Date();
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("HH:mm:ss dd/MM/YYYY");
		String now = formatter.format(today);
		date.add(0, now);
		
		}else{
			
			hist.add(0, (Cmd) this);
			Date today = new Date();
			SimpleDateFormat formatter;
			formatter = new SimpleDateFormat("HH:mm:ss dd/MM/YYYY");
			String now = formatter.format(today);
			date.add(0, now);
			hist.remove(10);
			date.remove(10);
				
		}
	}

	public static void clearHist() {
		hist.removeAll(hist);
		date.removeAll(date);
	}
	
	public static void affHist() {
		
		for (int i = hist.size()-1; i>=0 ; i--) {
			System.out.print(hist.get(i).getClass().getSimpleName().toLowerCase());
			System.out.println(" " + date.get(i));
		}
	}
	
}
