package outils;

import java.io.File;
import java.util.Scanner;

public class Clavier {
	private Scanner sc ;
	
	public Clavier(boolean interactif) throws Exception {
		if(interactif) {
			sc = new Scanner(System.in);
		} else {
			sc = new Scanner(new File("C:\\Users\\59013-57-14\\Desktop\\red\\cda-projet-cmd-rb\\src\\outils\\scenario.txt"));
		}
	}
	
	
	public String saisirString() {
		String saisie = sc.nextLine();
		return saisie;
	}
	
	public double saisirDouble() {
		double saisie = sc.nextDouble();
		sc.nextLine();
		return saisie;
	}
	
	public int saisirEntier() {
		int saisie = sc.nextInt();
		sc.nextLine();
		return saisie;
	}
	
	
}
