package outils;
import java.nio.file.Paths;

import cmdFini.AffHist;
import cmdFini.Cat;
import cmdFini.Cd;
import cmdFini.ClearHist;
import cmdFini.Copy;
import cmdFini.Crd;
import cmdFini.Crf;
import cmdFini.Dir;
import cmdFini.Dirng;
import cmdFini.Exit;
import cmdFini.Find;
import cmdFini.Getvar;
import cmdFini.Help;
import cmdFini.Isprime;
import cmdFini.Pwd;
import cmdFini.Quit;
import cmdFini.River;


public class Main {

	public static boolean dossier;

	public static void main(String[] args) throws Exception {

		boolean bool = true;
		String res = "";
		String param = "";
		Clavier monClavier = new Clavier(true);
		Cmd exe = null;
		dossier = Getvar.propPresent();
		Pwd curDir = new Pwd();

		while(bool) {

			System.out.println(" ");
			curDir.exec();
			System.out.print(">> ");
			res = monClavier.saisirString();
			param = res;
			res = res.toLowerCase();

			if(res.startsWith(River.NOM)) {
				exe = new River(param);
			} else if(res.startsWith(Isprime.NOM)) {
				exe = new Isprime(param);
			}else if(res.equals(Help.NOM)) {
				exe = new Help();
			}else if(res.equals(Exit.NOM)) {
				exe = new Exit();
			}else if(res.equals(Pwd.NOM)) {
				exe = new Pwd();
			}else if(res.equals(AffHist.NOM)) {
				exe = new AffHist();
			}else if(res.equals(ClearHist.NOM)) {
				exe = new ClearHist();
			}else if(res.equals(Dir.NOM)) {
				exe = new Dir();
			}else if(res.equals(Dirng.NOM)) {
				exe = new Dirng();
			}else if(res.equals(Quit.NOM)) {
				exe = new Quit();
			}else if(res.startsWith(Cd.NOM)) {
				exe = new Cd(param);
			}else if(res.split(" ")[0].equals(Find.NOM)) {
				exe = new Find(param);
			}else if(res.split(" ")[0].equals(Cat.NOM)) {
				exe = new Cat(param);
			}else if(res.split(" ")[0].equals((Copy.NOM))) {
				exe = new Copy(param);
			}else if(res.split(" ")[0].equals((Crf.NOM))) {
				exe = new Crf(param);
			}else if(res.split(" ")[0].equals((Crd.NOM))) {
				exe = new Crd(param);
			}else if(res.split(" ")[0].equals((Getvar.NOM))) {
				exe = new Getvar(param);
			}else if(res.equals("")){

			}else {
				System.out.println(res.split(" ")[0] + " n'est pas reconnu en tant que commande interne ou externe, un programme ex�cutable ou un fichier de commandes.");
			}

			if(exe == null) {

			} else{ 
				bool = exe.exec();
				if(exe instanceof History) {
					((History)exe).historiser();
				}
				exe = null;
			} 
		}	
	}
}
