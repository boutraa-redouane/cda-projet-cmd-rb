package outils;

public interface Cmd {
	
	public abstract boolean exec();
	
}
