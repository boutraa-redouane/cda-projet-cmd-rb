package cmdFini;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

import outils.Cmd;
import outils.History;

public class Crd extends History implements Cmd {
	
	public static final String NOM = "crd";
	private boolean bool = false;
	private String[] nom;

	
	public Crd(String param) {
		nom = param.split(" ");
		if(nom.length == 2) {
			bool = true;
		}
	}


	@Override
	public boolean exec() {
		if(bool) {
		try {
				Files.createDirectory(Paths.get((Pwd.getCurDir().toString()+"\\" + nom[1])));
		} catch(FileAlreadyExistsException e) {
			System.out.println("crd : Le nom " + nom[1] + " est deja utilis�");
		}catch (IOException e) {
			e.printStackTrace();
			System.out.println("crd : Path error : IOException");
		}
		}else {
			System.out.println("crd : l'argument n'est pas reconnu");
		}
		return true;
	}

}
