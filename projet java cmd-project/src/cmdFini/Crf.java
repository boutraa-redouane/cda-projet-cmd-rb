package cmdFini;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

import outils.Cmd;
import outils.History;

public class Crf extends History implements Cmd  {

	public static final String NOM = "crf";
	boolean bool=false;
	private String[] nom;



	public Crf(String param) {
		nom = param.split(" ");
		if(nom.length == 2){
			bool = true;
		}

	}



	@Override
	public boolean exec() {
		if (bool) {

			try {
				Files.createFile((Paths.get((Pwd.getCurDir().toString()+"\\" + nom[1]))));
			} catch(FileAlreadyExistsException e) {
				System.out.println("crf : Le fichier " + nom[1] + " est deja existant");
			}catch(AccessDeniedException e) {
				System.out.println("crf : un repertoire est deja nomm� " + nom[1]);
			}catch (IOException e) {
				e.printStackTrace();
				System.out.println("crf : Path error : IOException");
			}
		}else {
			System.out.println("crf : l'argument n'est pas reconnu");
		}

		return true;
	}

}
