package cmdFini;

import outils.Cmd;
import outils.History;

public class Quit extends History implements Cmd {

	public static final String NOM = "quit";
	public boolean exec() {
		return false;
	}
}
