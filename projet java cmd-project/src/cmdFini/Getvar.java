package cmdFini;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import outils.Cmd;
import outils.History;

public class Getvar extends History implements Cmd{

	public static String NOM = "getvars";
	private String[] arg;
	private boolean bool = false;
	private static Properties liste= System.getProperties();

	public Getvar(String param) {
		arg = param.split(" ");
		
	}



	@Override
	public boolean exec() {

		if(arg.length == 2 && arg[1].equals("-env")) {
			Map<String, String> env = System.getenv();
			System.out.println("\n-- listing env --");
			for (String envName : env.keySet()) {
				System.out.println(String.format("%-60s",envName)+String.format("%10s",env.get(envName)));
			}
			
		}else if(arg.length == 2 && arg[1].equals("-prop")) {
			liste.list(System.out);
		}else if(arg.length == 1){
			System.out.println("\n-- listing env --");
			Map<String, String> env = System.getenv();
			for (String envName : env.keySet()) {
				System.out.println(String.format("%-60s",envName)+String.format("%10s",env.get(envName)));
			}
			System.out.println("\n");
			Properties liste= System.getProperties();
			liste.list(System.out);
		}else {
			System.out.println("getvars : Argument error");
		}

		return true;
	}
	
	public static boolean propPresent() { 
		
		if(liste.containsKey("cdi.default.folder")) {
			String chemin = (String) liste.get("cdi.default.folder");
			if(Files.isDirectory(Paths.get(chemin))) {
				return true;
			}
		}
		return false;
		
	}
	
	public static Properties getListe() {
		return liste;
	}

}
