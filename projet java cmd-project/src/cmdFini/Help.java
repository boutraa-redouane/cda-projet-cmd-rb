package cmdFini;
import outils.Cmd;
import outils.History;

public class Help extends History implements Cmd{

	public static final String NOM = "help";

	public boolean exec() {
	
		

	
		System.out.println("- cat : lit un fichier entr� en parametre");
		System.out.println("- cd : change de repertoire");
		System.out.println("- copy : copie un fichier src vers un fichier dest");
		System.out.println("- CRD : cree un nouveau repertoire");
		System.out.println("- CRF : cree un nouveau fichier");
		System.out.println("- dir : affiche les repertoires et fichier du repertoire courant");
		System.out.println("- dirng : affiche les repertoires, les fichier du repertoire courant et leur nombres");
		System.out.println("- exit : quitte le programme");
		System.out.println("- isprime : indique si le nombre passer en parametre est premier ou non");
		System.out.println("- getvars : affiche la liste des variables environnement et la liste des proprietes de la jvm");
		System.out.println("- help : affiche la liste des commandes");
		System.out.println("- hist : affiche l'historique des commandes");
		System.out.println("- histclear : efface l'historique des commandes");
		System.out.println("- pwd : affiche le repertoire en cours");
		System.out.println("- river : affiche losrque les deux rivieres se rencontrent");
		
		return true;
	}
}
