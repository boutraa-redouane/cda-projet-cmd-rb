package cmdFini;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import outils.Cmd;
import outils.History;

public class Cd extends History implements Cmd {

	public static final String NOM = "cd";
	private String[] repertoire;
	private boolean bool = true;

	public Cd(String res) {
		repertoire = res.split(" ");
		if(repertoire.length == 2) {
			
		}else {
			bool = false;
		}


	}

	@Override
	public boolean exec() {
		Path path = Pwd.getCurDir();

		if(bool) {
			if(repertoire[1].equals("..")) {
				if(!path.toString().equals(path.getRoot().toString())) {
					Pwd.setCurDir(path.getParent());
				}

			}else if(Files.isDirectory(Paths.get(repertoire[1]))){
				Pwd.setCurDir(Paths.get(repertoire[1]));
			}else{
				try {
					bool = false;
					DirectoryStream<Path> stream = Files.newDirectoryStream(path);
					for (Path p : stream) {
						if(Files.isDirectory(p) && p.getFileName().toString().toLowerCase().equals(repertoire[1].toLowerCase())) {
							Pwd.setCurDir(p);
							bool = true;
							break;
						}
					}
				}catch(Exception e) {}
				
				if(!bool) {
					System.out.println("cd : repertoire inconnu");
				}

			}
		}else {
			System.out.println("cd : l'argument n'est pas reconnu");
		}

		return true;
	}
}
