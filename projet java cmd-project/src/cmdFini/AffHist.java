package cmdFini;

import outils.Cmd;
import outils.History;

public class AffHist implements Cmd{

	public static final String NOM = "hist";
	
	@Override
	public boolean exec() {
		History.affHist();
		return true;
	}

}
