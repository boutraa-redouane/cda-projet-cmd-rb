package cmdFini;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

import outils.Cmd;
import outils.History;

public class Copy extends History implements Cmd  {

	public static final String NOM = "copy";

	private String[] nom;
	boolean bool = false;


	public Copy(String param) {
		param.toLowerCase();
		nom = param.split(" ");
		if(nom.length == 3) {
			bool = true;
		}

	}


	@Override
	public boolean exec() {
		if(bool) {
			try {
				Files.copy(Paths.get((Pwd.getCurDir().toString()+"\\" + nom[1])), Paths.get((Pwd.getCurDir().toString()+"\\" + nom[2])));
			} catch(FileAlreadyExistsException e) {
				System.out.println("Le fichier " + nom[2] + " est deja existant");
			}catch (NoSuchFileException e) {
				System.out.println("Le fichier " + nom[2] + " n'existe pas");
			}catch (IOException e) {
				e.printStackTrace();
				System.out.println("Path error : IOException");
			}
		}else {
			System.out.println("copy : les arguments ne sont pas reconnu");
		}

		return true;
	}

}
