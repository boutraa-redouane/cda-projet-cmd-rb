package cmdFini;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import outils.Cmd;
import outils.History;

public class Dirng extends History implements Cmd {
	
	public static final String NOM = "dirng";

	@Override
	public boolean exec() {

		Path curDir = Paths.get("C://");
		DirectoryStream<Path> stream;
		int nbrRepertoire = 0;
		int nbrFichier = 0;
		
		try {
			stream = Files.newDirectoryStream(curDir);
			for (Path path : stream) {
				if(Files.isDirectory(path)) {
					nbrRepertoire++;
					System.out.println("<DIR> " + path);
				}else {
					System.out.println("      " + path);
					nbrFichier++;}
			}
			System.out.println(nbrFichier + " fichiers");
			System.out.println(nbrRepertoire+ " répertoires");
			stream.close();
		}catch (IOException e) {
			System.out.println("Path Error : IOExcption");
		}


		return true;
	}


}
