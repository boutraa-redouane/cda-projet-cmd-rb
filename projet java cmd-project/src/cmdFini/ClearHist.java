package cmdFini;
import outils.Cmd;
import outils.History;

public class ClearHist extends History implements Cmd {
	
	public static final String NOM = "clearhist";

	
	public boolean exec() {
		History.clearHist();
		return true;
	}
	
	

}
