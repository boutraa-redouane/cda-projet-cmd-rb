package cmdFini;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.AccessDeniedException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import outils.Cmd;
import outils.History;

public class Cat extends History implements Cmd {

	public static final String NOM = "cat";
	private String argument;


	public Cat(String param) {
		argument = param.split(" ")[1].toLowerCase();
	}


	@Override
	public boolean exec() {
		
		boolean bool = true;
		DirectoryStream<Path> stream;
		try {
			stream = Files.newDirectoryStream(Pwd.getCurDir());
			for (Path path : stream) {
				if(path.getFileName().toString().toLowerCase().equals(argument) ) {
					
					InputStream in = Files.newInputStream(path);
				    CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
				    decoder.onMalformedInput(CodingErrorAction.REPLACE);
				    Reader isReader = new InputStreamReader(in, decoder);
				    BufferedReader reader = new BufferedReader(isReader);
				    String line;
					System.out.println("***************** "+path.getFileName()+" ***************** ");
					while((line = reader.readLine()) != null) {
						System.out.println(line);
					}
					bool = false;
					System.out.println("***************** fin ***************** ");
					break;
				}
			}
		
		}catch(AccessDeniedException e) {
			System.out.println("cat : " + argument + " est un repertoire");
			bool = false;
		}catch(MalformedInputException e) {
			System.out.println("cat : erreur format du fichier inconnu");
			bool = false;
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("cat : IOException");
			bool = false;
		}
		
		if(bool) {
			System.out.println("cat : le fichier " + argument + " est introuvable");
		}

		return true;
	}

}
