package cmdFini;
import outils.Cmd;
import outils.History;

public class Exit extends History implements Cmd {

	public static final String NOM = "exit";
	public boolean exec() {
		return false;
	}
}
