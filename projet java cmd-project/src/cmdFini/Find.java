package cmdFini;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import outils.Cmd;
import outils.History;

public class Find extends History implements Cmd {

	public static final String NOM = "find";
	private String[] param;
	private boolean bool;

	public Find(String res) {
		res = res.toLowerCase();
		param = res.split(" ");
		if(param.length == 3 && (param[1].equals("-ends")||param[1].equals("-starts"))) {
			bool = true;
		}else if(param.length == 5 && param[1].equals("-starts") && param[3].equals("-ends")) {
			bool = true;
		}else if(param.length == 2){
			bool = true;
		}else {
			bool = false;
		}
	}

	@Override
	public boolean exec() {
		try {
			if(bool) {

				if(param.length == 2) {
					int a = afficheRecursive(Pwd.getCurDir(), param[1]);
					System.out.println(a + " fichier trouv�");
				}else if(param.length == 3) {
					if(param[1].equals("-starts")) {
						int a = afficheRecursive(Pwd.getCurDir(), param[2], true);
						System.out.println(a + " fichier trouv�");
					}else {
						int a = afficheRecursive(Pwd.getCurDir(), param[2], false);
						System.out.println(a + " fichier trouv�");
					}
				}else if(param.length == 5) {
					int a = afficheRecursive(Pwd.getCurDir(), param[2], param[4]);
					System.out.println(a + " fichier trouv�");
				}

			}else {
				System.out.println("Find : format incorrect de param�tre");
			}
		}catch(Exception e) {
			e.toString();
		}
		return true;
	}

	//**************************************************************
	private int afficheRecursive(Path chemin, String nom) throws Exception {

		int nbr = 0;

		DirectoryStream<Path> stream;

		stream = Files.newDirectoryStream(chemin);
		for (Path path : stream) {
			if(Files.isDirectory(path)) {
				nbr = nbr + afficheRecursive(path, nom);
			}else {
				String fin = path.getFileName().toString().toLowerCase();
				if(fin.contains(nom)) {
					nbr++;
					System.out.println(path);
				}
			}
		}
		stream.close();

		return nbr;

	}

	//***************************************************************

	private int afficheRecursive(Path chemin, String nom, boolean option) throws Exception {

		int nbr = 0;
		if(option) {
			DirectoryStream<Path> stream;

			stream = Files.newDirectoryStream(chemin);
			for (Path path : stream) {
				if(Files.isDirectory(path)) {
					nbr = nbr + afficheRecursive(path, nom, option);
				}else {
					String fin = path.getFileName().toString().toLowerCase();
					if(fin.startsWith(nom)) {
						nbr++;
						System.out.println(path);
					}
				}
			}
			stream.close();

			return nbr;

		}else {
			DirectoryStream<Path> stream;

			stream = Files.newDirectoryStream(chemin);
			for (Path path : stream) {
				if(Files.isDirectory(path)) {
					nbr = nbr + afficheRecursive(path, nom, option);
				}else {
					String fin = path.getFileName().toString().toLowerCase();
					if(fin.endsWith(nom)) {
						nbr++;
						System.out.println(path);
					}
				}
			}
			stream.close();

			return nbr;

		}

	}

	//*******************************************************************

	private int afficheRecursive(Path chemin, String nomDebut, String nomFin) throws Exception {

		int nbr = 0;

		DirectoryStream<Path> stream;

		stream = Files.newDirectoryStream(chemin);
		for (Path path : stream) {
			if(Files.isDirectory(path)) {
				nbr = nbr + afficheRecursive(path, nomDebut, nomFin);
			}else {
				String  fin = path.getFileName().toString().toLowerCase();
				if(fin.endsWith(nomFin) && fin.startsWith(nomDebut)) {
					nbr++;
					System.out.println(path);
				}
			}
		}
		stream.close();

		return nbr;

	}


}
