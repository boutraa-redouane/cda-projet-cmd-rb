package cmdFini;

import java.nio.file.Path;
import java.nio.file.Paths;

import outils.Cmd;
import outils.Main;

public class Pwd implements Cmd {
	
	public static final String NOM = "pwd";
	
	private static Path curDir = (Main.dossier ? Paths.get((String)Getvar.getListe().get("cdi.default.folder")) : Paths.get(System.getProperty("user.dir")));
	
	@Override
	public boolean exec() {
		System.out.println(curDir);
		return true;
	}

	public static Path getCurDir() {
		return curDir;
	}

	public static void setCurDir(Path curDir) {
		Pwd.curDir = curDir;
	}

	
	
	
}
