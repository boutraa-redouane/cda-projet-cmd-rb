package cmdFini;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import outils.Cmd;

public class Dir implements Cmd {
	
	public static final String NOM = "dir";

	@Override
	public boolean exec() {

		Path curDir = Pwd.getCurDir();
		DirectoryStream<Path> stream = null;
		try {
			stream = Files.newDirectoryStream(curDir);
			for (Path path : stream) {
				if(Files.isDirectory(path)) {
					System.out.println("<DIR> " + path);
				}else {System.out.println("      " + path);}

			}
		}catch (IOException e) {
			System.out.println("Path Error : IOException");
		} finally {
			try {
				if(stream != null) {
					stream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return true;
	} 

}
