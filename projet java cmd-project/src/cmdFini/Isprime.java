package cmdFini;
import outils.Cmd;

public class Isprime implements Cmd {
	
	public static final String NOM = "isprime";
	boolean arg1;
	private String[] args;
	
	public Isprime(String str) throws Exception{
		args = str.split(" ");
		arg1 = true;
		int a;
		
		try {
			a = Integer.parseInt(args[1]);
			
			if(a<0) {
				Exception e = new Exception("l'arguments doit etre positif");
				throw e;
			}

			if(args.length != 2) {
				IndexOutOfBoundsException e = new IndexOutOfBoundsException("Argument error");
				throw e;
			}
		}catch(IndexOutOfBoundsException e) {
			arg1 = false;
			System.out.println("Arguments error");
		}catch(Exception e) {
			arg1 = false;
			System.out.println(e.getMessage());
		}
	}

	public boolean exec() {
		
		if(arg1) {

			int n = Integer.parseInt(args[1]);
			int i,m=0,flag=0;         
			m=n/2;      
			if(n==0||n==1){  
				System.out.println("no");
			}else{  
				for(i=2;i<=m;i++){      
					if(n%i==0){      
						System.out.println("no");      
						flag=1;      
						break;      
					}      
				}      
				if(flag==0)  { System.out.println("yes"); }  
			}
			
		}else {
			System.out.println("l'arguments n'est pas reconnu");
		}
		return true;
	}
}



