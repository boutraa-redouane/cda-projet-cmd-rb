package cmdFini;
import outils.Cmd;

public class River implements Cmd {

	public static final String NOM = "river";
	private String[] args;
	boolean arg1;
	private int a;
	private int b;

	public River(String str) throws Exception{
		args = str.split(" ");
		arg1 = true;
		try {
			a = Integer.parseInt(args[1]);
			b = Integer.parseInt(args[2]);
			if(a<0 || b<0) {
				Exception e = new Exception("les arguments doivent etre positif");
				throw e;
			}

			if(args.length != 3) {
				IndexOutOfBoundsException e = new IndexOutOfBoundsException("les arguments doivent etre positif");
				throw e;
			}
		}catch(IndexOutOfBoundsException e) {
			arg1 = false;
			System.out.println("Arguments error");
		}catch(Exception e) {
			arg1 = false;
			System.out.println(e.getMessage());
		}
	}

	@Override
	public boolean exec() {

		if(arg1) {
			int r1 = Integer.parseInt(args[1]);
			int r2 = Integer.parseInt(args[2]);

			int res = 0;
			String intStr = "";

			while(r1!=r2){

				res =0;

				if(r1<r2){
					intStr = "" + r1;
					for(int i = 0; i<intStr.length();i++){
						res = res + intStr.charAt(i)-48;
					}
					r1 = r1 + res;
				}

				res =0;
				if(r1>r2){
					intStr = "" + r2;
					for(int i = 0; i< intStr.length();i++){
						res = res + intStr.charAt(i)-48;

					}
					r2 = r2 + res;
				}
			}	
			System.out.println(r1);
		}
		return true;
	}
}
